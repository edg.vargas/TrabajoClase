﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Ejercicio03Agosto
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
        public string Sumar(int num1, int num2)
        {
            int sum = (num1 + num2);
            return "El resultado es:  " + (sum).ToString(); 
        }

        public string Restar(int num1, int num2)
        {
            int sum = (num1 - num2);
            return "El resultado es:  " + (sum).ToString();
        }

        public string Multiplicar(int num1, int num2)
        {
            int sum = (num1 * num2);
            return "El resultado es:  " + (sum).ToString();
        }

        public string Dividir(double num1, double num2)
        {
            double sum = (num1 / num2);
            return "El resultado es:  " + sum.ToString(); 
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
